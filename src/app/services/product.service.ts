import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Datum, Image, Product, ProductDetail } from '../interfaces/product';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Authorization', `Bearer ${localStorage.getItem('token')}`);

  products = new BehaviorSubject<Datum[]>([]);
  imageDetail = new BehaviorSubject<any>([]);
  totalItem = new BehaviorSubject<number>(0);
  selectProduct = new BehaviorSubject<Datum>({
    title: '',
    location: '',
    description: '',
    price: 0,
    quota: 0,
    image: [],
    travel_code: '',
    departure_date: '',
    return_date: '',
  });

  constructor(private http: HttpClient) { }

  fetchProducts(page: number = 0, size: number = 10, query: string = '') {
    this.http.get<Product>(`${environment.apiUrl}/package`, {
      headers: this.headers,
      params: { page, size, query},
    }).subscribe((response) => {
      this.products.next(response.data)
      if (response.totalItem) {
        this.totalItem.next(response.totalItem)
      }
    })
  }

  getDetailProduct(travelCode: string) {
    this.http.get<ProductDetail>(`${environment.apiUrl}/package/${travelCode}`, {
      headers: this.headers
    }).subscribe((response) => {
      this.selectProduct.next(response.data)
      this.imageDetail.next(response.data.image)
    })
  }

  addEditProduct(postData: any, selectedPdt: any) {
    if(!selectedPdt){
      return this.http.post(`${environment.apiUrl}/package`, postData, {
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
    } else {
      return this.http.put(`${environment.apiUrl}/package/${selectedPdt}`, postData, {
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
    }
  }

  deleteProduct(travel_code: string){
    return this.http.patch(`${environment.apiUrl}/package/${travel_code}`, {}, {
      headers: this.headers
    });
  }

  deleteImageProduct(idImage: string) {
    return this.http.delete(`${environment.apiUrl}/images/${idImage}`, {
      headers: this.headers
    });
  }
}
