import { TestBed } from '@angular/core/testing';

import { TransactionService } from './transaction.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('TransactionService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [TransactionService]
  }));

  it('should be created', () => {
    const service: TransactionService = TestBed.get(TransactionService);
    expect(service).toBeTruthy();
  });

  it('should have getData function', () => {
    const service: TransactionService = TestBed.get(TransactionService);
    expect(service.getAllTransactions).toBeTruthy();
  });
});
