import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Advertisement, DataAdvertisement } from '../interfaces/advertisement';

@Injectable({
  providedIn: 'root'
})
export class AdvertisementService {
  images = new BehaviorSubject<DataAdvertisement[]>([])
  loading = new BehaviorSubject<boolean>(false)

  headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Authorization', `Bearer ${localStorage.getItem('token')}`);

  constructor(private http: HttpClient) { }

  getImagesAdv() {
    this.loading.next(true);
    this.http.get<Advertisement>(`${environment.apiUrl}/ads`, {
      headers: this.headers
    }).subscribe(resp => {
      this.images.next(resp.data)
      this.loading.next(false);
    })
  }

  addEditImage(postData: any, selectedPdt: any) {
    if(!selectedPdt){
      return this.http.post(`${environment.apiUrl}/ads`, postData ,{
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
    } else {
      return this.http.patch(`${environment.apiUrl}/ads/${selectedPdt.id}`, postData, {
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
    }
  }

  deleteImageAdv(id: number){
    return this.http.delete(`${environment.apiUrl}/ads/${id}`, {
      headers: this.headers
    });
  }
}
