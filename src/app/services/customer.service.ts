import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Customer, CustomerDetail, CustomerTransactionDetail } from '../interfaces/customer';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Authorization', `Bearer ${localStorage.getItem('token')}`);

  constructor(private http: HttpClient) { }

  getCustomers(page: number, size: number, query: string): Observable<Customer> {
    return this.http.get<Customer>(`${environment.apiUrl}/users`, {
      headers: this.headers,
      params: { page, size, query },
    });
  }

  getCustomerDetail(id: string): Observable<CustomerDetail> {
    return this.http.get<CustomerDetail>(`${environment.apiUrl}/users/${id}`, {
      headers: this.headers
    })
  }

  getCustomerHistoryTransaction(username: string): Observable<CustomerTransactionDetail> {
    return this.http.get<CustomerTransactionDetail>(`${environment.apiUrl}/users/${username}/transactions`, {
      headers: this.headers
    })
  }
}
