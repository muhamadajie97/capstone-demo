import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TransactionDetail, Transaction } from '../interfaces/transaction';

@Injectable({
  providedIn: 'root',
})
export class TransactionService {
  baseUrl = environment.apiUrl;
  token = localStorage.getItem('token');

  constructor(private http: HttpClient) {}

  getTransactions(
    page: number,
    size: number,
    query: string
  ): Observable<Transaction> {
    return this.http.get<Transaction>(`${this.baseUrl}/transactions`, {
      params: { page, size, query },
      headers: {
        Authorization: `Bearer ${this.token}`,
      },
    });
  }

  getAllTransactions() {
    return this.http.get<Transaction>(`${this.baseUrl}/transactions`, {
      headers: {
        Authorization: `Bearer ${this.token}`,
      },
    });
  }

  getTransactionById(id: string) {
    return this.http.get<TransactionDetail>(
      `${this.baseUrl}/transactions/${id}`,
      {
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
      }
    );
  }
}
