import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Payment } from '../interfaces/payment';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class PaymentService {
  constructor(private http: HttpClient) {}

  baseUrl = environment.apiUrl;
  token = localStorage.getItem('token');

  getAllPayment(): Observable<Payment> {
    return this.http.get<Payment>(`${this.baseUrl}/payment-methods`, {
      headers: {
        Authorization: `Bearer ${this.token}`,
      },
    });
  }

  patchState(id: string, state: boolean) {
    return this.http.patch<any>(
      `${this.baseUrl}/payment-methods/${id}`,
      {
        is_active: state,
      },
      {
        headers: {
          Authorization: `Bearer ${this.token}`,
        },
      }
    );
  }
}
