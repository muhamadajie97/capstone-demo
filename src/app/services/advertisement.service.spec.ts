import { TestBed } from '@angular/core/testing';

import { AdvertisementService } from './advertisement.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AdvertisementService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [AdvertisementService]
  }));

  it('should be created', () => {
    const service: AdvertisementService = TestBed.get(AdvertisementService);
    expect(service).toBeTruthy();
  });

  it('should have getData function', () => {
    const service: AdvertisementService = TestBed.get(AdvertisementService);
    expect(service.getImagesAdv).toBeTruthy();
  });
});
