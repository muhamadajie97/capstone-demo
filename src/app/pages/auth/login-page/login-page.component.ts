import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  loadingSubmit = true;

  constructor(private router: Router, private authService: AuthService, private messageService: MessageService) { }

  ngOnInit(): void {
  }

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
  });

  get email() {return this.loginForm.get('email')};
  get password() {return this.loginForm.get('password')};

  clear(){
    this.loginForm.reset();
  }

  submit(){
    const {email, password} = this.loginForm.value;
    this.loadingSubmit = false;

    if(email !== '' && password !== ''){
      this.authService.loginDashboard({email, password}).subscribe({
        next: (res: any) => {
          if(res.status === 'Success') {
            this.loadingSubmit = true
            this.router.navigate([''])
            localStorage.setItem('token', res.data)
          }
        },
        error: () => {
          this.loadingSubmit = true
          this.messageService.add({key: 'mKey1', severity:'info', summary: 'Info', detail: `Wrong email or password`});
        },
      })
    } else {
      this.loadingSubmit = true
      this.messageService.add({key: 'mKey1', severity:'info', summary: 'Info', detail: `Email and password does't empty`});
    }
  }
}
