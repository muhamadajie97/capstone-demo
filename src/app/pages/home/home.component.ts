import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DataDashboard } from 'src/app/interfaces/dashboard';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  dataDashboard = new BehaviorSubject<DataDashboard>({
    active_user: 0,
    product_count: 0,
    transaction_sum_income: 0,
    transaction_per_day: {
      count_pending: 0,
      count_success: 0,
      count_failed: 0,
      count_expired: 0,
      count_total_transaction: 0,
    },
  });

  chartData = new BehaviorSubject<any>({});

  data = [
    {
      title: 'Active User',
      value: 0,
      icon: 'fa-user-friends',
      bgColor: 'bg-purple',
      color: 'text-purple',
      size: 'fs-50',
    },
    {
      title: 'Transaction',
      value: 0,
      icon: 'fa-arrow-right-arrow-left',
      bgColor: 'bg-yellow',
      color: 'text-yellow',
      size: 'fs-50',
    },
    {
      title: 'Income',
      value: '',
      icon: 'fa-hand-holding-dollar',
      bgColor: 'bg-red',
      color: 'text-red',
      size: 'fs-20',
    },
    {
      title: 'Total Package',
      value: 0,
      icon: 'fa-clipboard-list',
      bgColor: 'bg-green',
      color: 'text-green',
      size: 'fs-50',
    },
  ];

  options = {
    plugins: {
      title: {
        display: true,
        text: 'Transaction',
        fontSize: 16,
      },
      legend: {
        position: 'bottom',
      },
    },
  };

  loading: boolean = false;

  getFailed: boolean = false;

  constructor(private service: DashboardService) {}

  ngOnInit(): void {
    this.getAllData();

    this.dataDashboard.subscribe((data) => {
      this.chartData.next({
        labels: ['Success', 'Pending', 'Failed', 'Expired'],
        datasets: [
          {
            data: [
              data.transaction_per_day.count_success,
              data.transaction_per_day.count_pending,
              data.transaction_per_day.count_failed,
              data.transaction_per_day.count_expired,
            ],
          },
        ],
      });
    });
  }

  getAllData() {
    this.loading = true;
    this.service.getAllDashboard().subscribe({
      next: (resp) => {
        this.dataDashboard.next(resp.data);
        this.data[0].value = resp.data.active_user;
        this.data[1].value =
          resp.data.transaction_per_day.count_total_transaction;
        this.data[2].value = resp.data.transaction_sum_income ?? 0;
        this.data[3].value = resp.data.product_count;
        this.loading = false;
      },
      error: () => {
        this.getFailed = true;
        this.loading = false;
      },
    });
  }
}
