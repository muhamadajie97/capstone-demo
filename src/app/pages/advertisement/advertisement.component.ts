import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DataAdvertisement } from 'src/app/interfaces/advertisement';
import { AdvertisementService } from 'src/app/services/advertisement.service';

@Component({
  selector: 'app-advertisement',
  templateUrl: './advertisement.component.html',
  styleUrls: ['./advertisement.component.css']
})
export class AdvertisementComponent implements OnInit {
  images: Observable<DataAdvertisement[]> = this.advertisementService.images;
  isLoading: Observable<boolean> = this.advertisementService.loading;
  displayAddEditModal = false;
  selectedImage: any = null;

  constructor(private advertisementService: AdvertisementService, private confirmationService: ConfirmationService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.advertisementService.getImagesAdv()
  }

  showAddModal() {
    this.displayAddEditModal = true;
    this.selectedImage = null;
  }

  hideAddModal(isClosed: boolean) {
    this.displayAddEditModal = !isClosed;
  }

  showEditModal(image: DataAdvertisement) {
    this.displayAddEditModal = true;
    this.selectedImage = image;
  }

  deleteImage(idImage: number) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this image?',
      accept: () => {
        this.advertisementService.deleteImageAdv(idImage).subscribe({
          next: (res: any) => {
            if(res.status === 'Success') {
              this.advertisementService.getImagesAdv()
              this.messageService.add({key: 'mKey1', severity:'success', summary: 'Success', detail: 'Image Deleted Successfully'});
            }
          },
          error: (err) => {
            this.messageService.add({key: 'mKey1', severity:'error', summary: 'Failed Delete Image', detail: err.statusText});
          }
        })
      }
    });
  }
}
