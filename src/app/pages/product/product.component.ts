import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Datum } from '../../interfaces/product';
import { ConfirmationService, LazyLoadEvent, MessageService } from 'primeng/api';
import { DetailProductComponent } from 'src/app/components/detail-product/detail-product.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  cols: any[] = [];
  products: Datum[] = [];
  displayAddEditModal = false;
  displayDetailModal = false;

  totalRecords: number = 0;
  pageNumber: number = 0;
  loading: boolean = true;

  query = '';
  visible = true;

  @ViewChild(DetailProductComponent, { static: true })
  child : DetailProductComponent | undefined;

  constructor(private productService: ProductService, private confirmationService: ConfirmationService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.cols = [
      { field: 'title', header: 'Title' },
      { field: 'location', header: 'Location' },
      { field: 'price', header: 'Price' },
      { field: 'quota', header: 'Quota' },
      { field: '', header: 'Actions' },
    ];
  }

  loadCustomers(event: LazyLoadEvent) {
    this.loading = true;
    event.first == 0
      ? (this.pageNumber = 0)
      : (this.pageNumber = (event.first ?? 10) / (event.rows ?? 10)) + 1;
    setTimeout(() => {
      this.productService.fetchProducts(this.pageNumber, event.rows ?? 10, this.query)
      this.productService.products.subscribe(products => {
        this.products = products;
      })
      this.productService.totalItem.subscribe(totalItem => {
        this.totalRecords = totalItem;
      })

      window.scrollTo(0, 0);
      this.loading = false;
    }, 1000);
  }

  showAddModal() {
    this.displayAddEditModal = true;
    this.productService.selectProduct.next({
      title: '',
      location: '',
      description: '',
      price: 0,
      quota: 0,
      image: [],
      travel_code: '',
      departure_date: '',
      return_date: '',
    })
  }

  hideAddModal(isClosed: boolean) {
    this.displayAddEditModal = !isClosed;
  }

  showDetailModal(id: string) {
    this.displayDetailModal = true;
    this.child?.getProductDetail(id);
  }

  hideDetailModal(isClosed: boolean) {
    this.displayDetailModal = !isClosed;
  }

  showEditModal(product: any) {
    this.displayAddEditModal = true;
    this.productService.getDetailProduct(product.travel_code)
  }

  searchId(event: any) {
    this.query = event.target.value;
  }

  search() {
    this.visible = false;
    setTimeout(() => {
      this.visible = true;
    }, 1000);
  }

  deleteProduct(product: string) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete this product?',
      accept: () => {
        this.productService.deleteProduct(product).subscribe({
          next: (response: any) => {
            if(response.status === 'Success'){
              this.products = this.products.filter(data => data.travel_code !== product);
              this.messageService.add({severity:'success', summary: 'Success', detail: 'Product Deleted Successfully'});
            }
          },
          error: (err) => {
            this.messageService.add({severity:'error', summary: 'Error Info', detail: err.statusText});
          }}
        )
      }
    });
  }
}
