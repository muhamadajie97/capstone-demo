import { Component, OnInit, ViewChild } from '@angular/core';
import { LazyLoadEvent, MessageService } from 'primeng/api';
import { DetailCustomerComponent } from 'src/app/components/detail-customer/detail-customer.component';
import { DataCustomer } from 'src/app/interfaces/customer';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  cols: any[] = [];
  customers: DataCustomer[] = [];
  displayDetailCustomerModal = false;

  totalRecords: number = 0;
  pageNumber: number = 0;
  loading: boolean = true;

  query = '';
  visible = true;

  @ViewChild(DetailCustomerComponent, { static: true })
  child : DetailCustomerComponent | undefined;

  constructor(private customerService: CustomerService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.cols = [
      { field: 'email', header: 'Email' },
      { field: 'username', header: 'Username' },
      { field: 'fullname', header: 'Fullname' },
      { field: 'dob', header: 'Date of Birth' },
      { field: 'gender', header: 'Gender' },
      { field: '', header: 'Action' },
    ];
  }

  showDetailModal(id: string) {
    this.displayDetailCustomerModal = true;
    this.child?.getCustomerDetail(id);
  }

  hideDetailModal(isClosed: boolean) {
    this.displayDetailCustomerModal = !isClosed;
  }

  loadCustomers(event: LazyLoadEvent) {
    this.loading = true;
    event.first == 0
      ? (this.pageNumber = 0)
      : (this.pageNumber = (event.first ?? 10) / (event.rows ?? 10)) + 1;
    setTimeout(() => {
      this.customerService
        .getCustomers(this.pageNumber, event.rows ?? 10, this.query)
        .subscribe({
          next: (response: any) => {
            if (response.data) {
              this.customers = response.data;
              this.totalRecords = response.totalItem;
            }
          },
          error: (err) => {
            this.messageService.add({key: 'mKey1', severity:'error', summary: 'Error Info', detail: err.statusText});
          }
        }
        );
      window.scrollTo(0, 0);
      this.loading = false;
    }, 1000);
  }

  searchId(event: any) {
    this.query = event.target.value;
  }

  search() {
    this.visible = false;
    setTimeout(() => {
      this.visible = true;
    }, 1000);
  }
}