import { Component, OnInit } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { Datum, TransactionDetail } from 'src/app/interfaces/transaction';
import { TransactionService } from 'src/app/services/transaction.service';
import * as FileSaver from 'file-saver-es';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css'],
})
export class TransactionComponent implements OnInit {
  data$: Datum[] = [];

  exportTransactions: any = [];

  exportCols = [
    {
      field: 'invoice_number',
      header: 'Invoice Number',
    },
    { field: 'transaction_date', header: 'Transaction Date' },
    { field: 'payment_date', header: 'Payment Date' },
    { field: 'payment_status', header: 'Payment Status' },
  ];

  exportData = this.exportCols.map((col) => ({
    title: col.header,
    dataKey: col.field,
  }));

  detailTrans: TransactionDetail = {
    status: '',
    message: '',
    data: {
      title: '',
      fullname: '',
      group_member: [
        {
          fullname: '',
          phoneNumber: '',
          nik: '',
        },
      ],
      payment_method_types: '',
      payment_status: '',
      payment_link: '',
      invoice_id: '',
      ticket_qty: 0,
      total_amount: 0,
    },
  };

  invoiceNum: string = '';

  totalRecords: number = 0;

  pageNumber: number = 0;

  loading: boolean = true;

  displayModal: boolean = false;

  loadExcel: boolean = false;

  query = '';

  visible = true;

  emptyMessage = 'No records found';

  constructor(private service: TransactionService) {}

  ngOnInit(): void {}

  loadTransaction(event: LazyLoadEvent) {
    this.loading = true;
    event.first == 0
      ? (this.pageNumber = 0)
      : (this.pageNumber = (event.first ?? 10) / (event.rows ?? 10)) + 1;

    setTimeout(() => {
      this.service
        .getTransactions(this.pageNumber, event.rows ?? 10, this.query)
        .subscribe({
          next: (resp) => {
            if (resp.data) {
              this.totalRecords = resp.totalItem;
              this.data$ = resp.data;
              this.loading = false;
            }
          },
          error: (error) => {
            this.emptyMessage = error.error.error;
            this.loading = false;
          },
        });
      window.scrollTo(0, 0);
    }, 1000);
  }

  showModal(data: string) {
    this.displayModal = true;
    this.invoiceNum = data;
    this.service.getTransactionById(data).subscribe((resp) => {
      this.detailTrans = resp;
    });
  }

  searchId(event: any) {
    this.query = event.target.value;
  }

  search() {
    this.visible = false;
    setTimeout(() => {
      this.visible = true;
    }, 1000);
  }

  exportExcel() {
    this.loadExcel = true;
    this.service.getAllTransactions().subscribe({
      next: (resp) => {
        if (resp.data) {
          this.exportTransactions = resp.data;
          import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(this.exportTransactions);
            const workbook = {
              Sheets: { data: worksheet },
              SheetNames: ['data'],
            };
            const excelBuffer: any = xlsx.write(workbook, {
              bookType: 'xlsx',
              type: 'array',
            });
            this.saveAsExcelFile(excelBuffer, 'transactions');
          });
        }
        this.loadExcel = false;
      },
      error: () => {
        this.loadExcel = false;
      },
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    FileSaver.saveAs(
      data,
      fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION
    );
  }
}
