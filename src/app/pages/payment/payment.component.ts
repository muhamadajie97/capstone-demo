import { Component, OnInit } from '@angular/core';
import { dataPayment } from 'src/app/interfaces/payment';
import { PaymentService } from 'src/app/services/payment.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css'],
})
export class PaymentComponent implements OnInit {
  loading: boolean = false;
  stateLoad: boolean = false;
  idVa: string = '';
  stateVa: boolean = true;
  errorStatus: string = '';
  errorMessage: string = '';
  failedGet: boolean = false;

  dataPayment: dataPayment[] = [];
  constructor(
    private paymentService: PaymentService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getPayment();
  }

  showSuccess() {
    this.messageService.add({
      severity: 'success',
      summary: 'Success',
      detail: 'Berhasil Diubah!',
    });
  }

  showFailed() {
    this.messageService.add({
      severity: 'error',
      summary: `Error <${this.errorStatus}>`,
      detail: 'Gagal Mengubah Status!',
    });
  }

  getPayment() {
    this.loading = true;
    this.paymentService.getAllPayment().subscribe({
      next: (resp) => {
        if (resp.data) {
          this.dataPayment = resp.data;
          this.loading = false;
        }
      },
      error: (error) => {
        this.loading = false;
        this.failedGet = true;
        this.errorStatus = error.statusText;
      },
    });
  }

  changeState(id: string, state: boolean) {
    this.stateLoad = true;
    this.paymentService.patchState(id, state).subscribe({
      next: (resp) => {
        setTimeout(() => {
          if (resp.status == 'Success') {
            this.stateLoad = false;
            this.getPayment();
            this.showSuccess();
          }
        }, 1000);
      },
      error: (error) => {
        setTimeout(() => {
          if (error) {
            this.stateLoad = false;
            this.errorStatus = error.status;
            this.getPayment();
            this.showFailed();
          }
        }, 1000);
      },
    });
  }
}
