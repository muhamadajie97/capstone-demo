import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from './components/navbar/navbar.component';

import { HomeComponent } from './pages/home/home.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { ProductComponent } from './pages/product/product.component';
import { CustomerComponent } from './pages/customer/customer.component';
import { TransactionComponent } from './pages/transaction/transaction.component';
import { LoginPageComponent } from './pages/auth/login-page/login-page.component';
import { AdvertisementComponent } from './pages/advertisement/advertisement.component';
import { PermissionGuard } from './helpers/permission.guard';
import { NotfoundComponent } from './pages/notfound/notfound.component';

const routes: Routes = [
  { path: 'login', component: LoginPageComponent, canActivate: [PermissionGuard], },
  {
    path: '',
    component: NavbarComponent,
    canActivate: [PermissionGuard],
    children: [
      {
        path: '',
        component: HomeComponent,
        canActivate: [PermissionGuard],
      },
      {
        path: 'product',
        component: ProductComponent,
        canActivate: [PermissionGuard],
      },
      {
        path: 'customer',
        component: CustomerComponent,
        canActivate: [PermissionGuard],
      },
      {
        path: 'transaction',
        component: TransactionComponent,
        canActivate: [PermissionGuard],
      },
      {
        path: 'payment',
        component: PaymentComponent,
        canActivate: [PermissionGuard],
      },
      {
        path: 'advertisement',
        component: AdvertisementComponent,
        canActivate: [PermissionGuard],
      },
    ],
  },
  { path: '**', component: NotfoundComponent,}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
