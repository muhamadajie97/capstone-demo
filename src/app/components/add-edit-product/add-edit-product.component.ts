import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../../services/product.service';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { Datum } from 'src/app/interfaces/product';

@Component({
  selector: 'app-add-edit-product',
  templateUrl: './add-edit-product.component.html',
  styleUrls: ['./add-edit-product.component.css']
})
export class AddEditProductComponent implements OnInit, OnChanges {
  @Input() displayAddEditModal: boolean = true;
  @Output() clickClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  selectedProduct: Observable<Datum> = this.productService.selectProduct;

  editProduct: Datum = {
    title: '',
    location: '',
    description: '',
    price: 0,
    quota: 0,
    image: [],
    travel_code: '',
    departure_date: '',
    return_date: '',
  };
  modalType = 'Add';
  loadingSubmit = true;
  firstDateValue!: Date;
  minimumDateDeparture = new Date();
  minimumDateReturn!: Date;
  uploadedFiles: any[] = [];
  newDataImage: any[] = [];

  productForm = this.fb.group({
    title: ["", Validators.required],
    location: ["", Validators.required],
    price: [0, Validators.required],
    quota: [0, Validators.required],
    description: ["", Validators.required],
    image: [""],
    departure_date: ["", Validators.required],
    return_date: [{value: new Date(), disabled: true}, Validators.required],
  })

  constructor(private fb: FormBuilder, private productService: ProductService, private messageService: MessageService) { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.selectedProduct.subscribe(product => {
      if(product.title.length > 0){
        this.modalType = 'Edit';
        this.productForm.get('return_date')?.enable();
        this.selectedProduct.subscribe(resp => {
          this.newDataImage  = resp.image.filter((image : any) => image.path !== "error")
          this.editProduct = resp;
          this.productForm.patchValue({
            title: resp.title,
            location: resp.location,
            description: resp.description,
            price: resp.price,
            quota: resp.quota,
            departure_date: resp.departure_date,
            return_date: new Date(resp.return_date),
          });
        })

        this.minimumDateReturn = new Date(this.productForm.value.departure_date!)
      } else {
        this.productForm.reset();
        this.modalType = 'Add';
        this.productForm.get('return_date')?.disable();
      }
    })
  }

  selectFirstDate(val: Date){
    this.productForm.patchValue({return_date : val!})
    this.minimumDateReturn = val;
    this.productForm.get('return_date')?.enable();
  }

  closeModal() {
    this.productForm.get('return_date')?.disable();
    this.productForm.reset();
    this.clickClose.emit(true);
  }

  fileImage: any = null

  myUploader(event: any) {
    if(event.files.length > 0){
      for(let file of event.files) {
        this.uploadedFiles.push(file);
      }
    }
  }

  addEditProduct() {
    this.loadingSubmit = false;
    const dateDeparture = new Date(this.productForm.value.departure_date ?? "");
    const departureDateConvert = (`${dateDeparture.getFullYear()}-${dateDeparture.getMonth().toString().length === 1 ? '0' + String(dateDeparture.getMonth()+1) : dateDeparture.getMonth()+1}-${dateDeparture.getDate().toString().length === 1 ? '0' + String(dateDeparture.getDate()) : dateDeparture.getDate()}`)

    const dateReturn = new Date(this.productForm.value.return_date ?? "");
    const returnDateConvert = (`${dateReturn.getFullYear()}-${dateReturn.getMonth().toString().length === 1 ? '0' + String(dateReturn.getMonth()+1) : dateReturn.getMonth()+1}-${dateReturn.getDate().toString().length === 1 ? '0' + String(dateReturn.getDate()) : dateReturn.getDate()}`)

    const fd = new FormData();
    fd.append(this.modalType == 'Add' ? 'packageRequestDTO' : 'updatePackageRequestDTO', new Blob([JSON.stringify({
      title: this.productForm.value.title,
      location: this.productForm.value.location,
      description: this.productForm.value.description,
      price: this.productForm.value.price,
      quota: this.productForm.value.quota,
      departure_date: departureDateConvert,
      return_date: returnDateConvert,
    })], {
      type: 'application/json'
    }));
    fd.append('file1', this.uploadedFiles[0] ?? '', this.uploadedFiles[0].name ?? "");
    fd.append('file2', this.uploadedFiles[1] ?? '', this.uploadedFiles[1].name ?? "");
    fd.append('file3', this.uploadedFiles[2] ?? '', this.uploadedFiles[2].name ?? "");

    if(this.uploadedFiles[3]){
      fd.append('file4', this.uploadedFiles[3] ?? '', this.uploadedFiles[3].name ?? "");
    } else {
      fd.append('file4', new Blob([JSON.stringify(null)]));
    }

    if(this.uploadedFiles[4]){
      fd.append('file5', this.uploadedFiles[4] ?? '', this.uploadedFiles[4].name ?? "");
    } else {
      fd.append('file5', new Blob([JSON.stringify(null)]));
    }

    const addImageProduct = this.productService.addEditProduct(fd, this.editProduct.travel_code);

    if(this.productForm.value.title === '' && this.productForm.value.title === '' && this.productForm.value.title === '' && this.productForm.value.title === '' && this.productForm.value.title === '') {
      this.loadingSubmit = true;
      this.messageService.add({severity:'info', summary: 'Info', detail: 'Please insert all field value'});
    } else {
      addImageProduct.subscribe({
        next: (response: any) => {
          if(response){
            this.loadingSubmit = true;
            this.closeModal();
            this.productService.fetchProducts();
            const msg = this.modalType === 'Add' ? 'Image added' : 'Image updated';
            this.messageService.add({severity:'success', summary: 'Success', detail: msg});
          }
        },
        error: (err: any) => {
          this.loadingSubmit = true;
          this.messageService.add({severity:'error', summary: 'Error Info', detail: err.statusText});
        }
      })
    }
  }

  deleteImage(id: string){
    this.productService.deleteImageProduct(id).subscribe({
      next: (response: any) => {
        if(response){
          this.selectedProduct.subscribe(product => {
            this.productService.getDetailProduct(product.travel_code);
          })
          this.messageService.add({severity:'success', summary: 'Success', detail: 'Image deleted successfully'});
        }
      },
      error: (err: any) => {
        this.messageService.add({severity:'error', summary: 'Error Info', detail: err.statusText});
      }
    })
  }
}
