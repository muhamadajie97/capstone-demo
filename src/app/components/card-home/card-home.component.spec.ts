import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardHomeComponent } from './card-home.component';
import { CurrencyFormatPipe } from 'src/app/pipes/currency-format.pipe';


describe('CardHomeComponent', () => {
  let component: CardHomeComponent;
  let fixture: ComponentFixture<CardHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardHomeComponent, CurrencyFormatPipe ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CardHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
