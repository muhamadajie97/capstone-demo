import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomerTransactionStatus } from 'src/app/interfaces/customer';
import { CustomerService } from 'src/app/services/customer.service';
import { LazyLoadEvent, MessageService } from 'primeng/api';

@Component({
  selector: 'app-detail-customer',
  templateUrl: './detail-customer.component.html',
  styleUrls: ['./detail-customer.component.css']
})
export class DetailCustomerComponent implements OnInit {
  cols: any[] = [];
  customerDetail: any;
  customersHistory: CustomerTransactionStatus [] = [];

  totalRecords: number = 0;
  pageNumber: number = 0;
  loading: boolean = true;

  @Input() username: string = "";
  @Input() displayDetailCustomerModal: boolean = true;
  @Output() clickClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private customerService: CustomerService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.cols = [
      { field: 'title', header: 'Destination' },
      { field: 'departure_date', header: 'Departure Date' },
      { field: 'return_date', header: 'Return Date' },
      { field: 'payment_status', header: 'Status Payment' }
    ];
  }

  closeModalDetailCustomer() {
    this.clickClose.emit(true);
  }

  getCustomerDetail(id: string) {
    this.customerService.getCustomerDetail(id).subscribe({
      next: (response) => {
        this.customerDetail = response.data;
      },
      error: (err) => {
        this.messageService.add({severity:'error', summary: 'Error Info', detail: err.statusText});
      }}
    )

    this.customerService.getCustomerHistoryTransaction(id).subscribe(
      {
        next: (response) => {
          this.customersHistory = response.data;
        },
        error: (err) => {
          this.messageService.add({severity:'error', summary: 'Error Info', detail: err.statusText});
        }
      })
  }
}
