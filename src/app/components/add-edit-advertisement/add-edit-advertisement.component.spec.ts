import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditAdvertisementComponent } from './add-edit-advertisement.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MessageService } from 'primeng/api';

describe('AddEditAdvertisementComponent', () => {
  let component: AddEditAdvertisementComponent;
  let fixture: ComponentFixture<AddEditAdvertisementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
      ],
      declarations: [ AddEditAdvertisementComponent ],
      providers: [MessageService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddEditAdvertisementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
