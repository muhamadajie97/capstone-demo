import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { AdvertisementService } from 'src/app/services/advertisement.service';

@Component({
  selector: 'app-add-edit-advertisement',
  templateUrl: './add-edit-advertisement.component.html',
  styleUrls: ['./add-edit-advertisement.component.css']
})
export class AddEditAdvertisementComponent implements OnInit, OnChanges {
  @Input() displayAddEditModal: boolean = true;
  @Input() selectedImage: any = null;
  @Output() clickClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  modalType = 'Add';
  loadingSubmit = true;

  imageForm = this.fb.group({
    name: ["", Validators.required],
    image: ["", Validators.required],
  })

  constructor(private fb: FormBuilder, private advertisementService: AdvertisementService, private messageService: MessageService) { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    if(this.selectedImage){
      this.modalType = 'Edit';
      this.imageForm.patchValue(this.selectedImage);
    } else {
      this.modalType = 'Add';
      this.imageForm.reset();
    }
  }

  closeModal() {
    this.imageForm.reset();
    this.clickClose.emit(true);
  }

  fileImage: any = null

  uploadImage(event: any) {
    if(event.files.length > 0){
      const file = event.files[0]
      this.imageForm.patchValue({
        image: file
      })
      this.fileImage = file
    }
  }

  addEditImage() {
    this.loadingSubmit = false
    const fd = new FormData();
    fd.append('advertisementRequestDTO', new Blob([JSON.stringify({name: this.imageForm.value.name})], {
      type: 'application/json'
    }));
    fd.append("File", this.imageForm.value.image ?? '', this.fileImage?.name ?? "");

    const addImageContent = this.advertisementService.addEditImage(fd, this.selectedImage);

    if(this.imageForm.value.name === '' && this.fileImage === '') {
      this.loadingSubmit = true;
      this.messageService.add({severity:'info', summary: 'Info', detail: 'Please insert all field value'});
    } else {
      addImageContent.subscribe({
        next: (response: any) => {
          if(response){
            this.loadingSubmit = true;
            this.advertisementService.getImagesAdv()
            this.closeModal();
            const msg = this.modalType === 'Add' ? 'Image added' : 'Image updated';
            this.messageService.add({severity:'success', summary: 'Success', detail: msg});
          }
        },
        error: (err) => {
          this.loadingSubmit = true;
          this.messageService.add({severity:'error', summary: 'Error Info', detail: err.statusText});
        }
      })
    }
  }
}
