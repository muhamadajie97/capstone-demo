import { Component, Input, OnInit } from '@angular/core';
import { TransactionDetail } from 'src/app/interfaces/transaction';

@Component({
  selector: 'app-detail-transaction',
  templateUrl: './detail-transaction.component.html',
  styleUrls: ['./detail-transaction.component.css'],
})
export class DetailTransactionComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  @Input() props: TransactionDetail = {
    status: '',
    message: '',
    data: {
      title: '',
      fullname: '',
      group_member: [
        {
          fullname: '',
          phoneNumber: '',
          nik: '',
        },
      ],
      payment_method_types: '',
      payment_status: '',
      payment_link: '',
      invoice_id: '',
      ticket_qty: 0,
      total_amount: 0,
    },
  };
}
