import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailTransactionComponent } from './detail-transaction.component';
import { CurrencyFormatPipe } from 'src/app/pipes/currency-format.pipe';

describe('DetailTransactionComponent', () => {
  let component: DetailTransactionComponent;
  let fixture: ComponentFixture<DetailTransactionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailTransactionComponent, CurrencyFormatPipe ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetailTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
