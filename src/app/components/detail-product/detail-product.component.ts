import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Datum, Image } from 'src/app/interfaces/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['./detail-product.component.css']
})
export class DetailProductComponent implements OnInit {
  images?:  Observable<any> = this.productService.imageDetail;
  detailProduct: Observable<Datum> = this.productService.selectProduct;

  @Input() displayDetailModal: boolean = true;
  @Output() clickClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
  }

  getProductDetail(id: string) {
    this.productService.getDetailProduct(id)
  }

  closeModal() {
    this.clickClose.emit(true);
  }
}
