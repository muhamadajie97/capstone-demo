// Angular Module
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Components
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { ProductComponent } from './pages/product/product.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { CustomerComponent } from './pages/customer/customer.component';
import { CardHomeComponent } from './components/card-home/card-home.component';
import { TransactionComponent } from './pages/transaction/transaction.component';
import { LoginPageComponent } from './pages/auth/login-page/login-page.component';
import { AdvertisementComponent } from './pages/advertisement/advertisement.component';
import { DetailProductComponent } from './components/detail-product/detail-product.component';
import { DetailCustomerComponent } from './components/detail-customer/detail-customer.component';
import { AddEditProductComponent } from './components/add-edit-product/add-edit-product.component';
import { DetailTransactionComponent } from './components/detail-transaction/detail-transaction.component';
import { AddEditAdvertisementComponent } from './components/add-edit-advertisement/add-edit-advertisement.component';

// NgBootstrap Module
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// PrimeNG Module
import { TagModule } from 'primeng/tag';
import { CardModule } from 'primeng/card';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { ChartModule } from 'primeng/chart';
import { EditorModule } from 'primeng/editor';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { DividerModule } from 'primeng/divider';
import { TooltipModule } from 'primeng/tooltip';
import { CalendarModule } from 'primeng/calendar';
import { SkeletonModule } from 'primeng/skeleton';
import { GalleriaModule } from 'primeng/galleria';
import { PasswordModule } from 'primeng/password';
import { AccordionModule } from 'primeng/accordion';
import { InputTextModule } from 'primeng/inputtext';
import { FileUploadModule } from 'primeng/fileupload';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputNumberModule } from 'primeng/inputnumber';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

// Services
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { ProductService } from './services/product.service';

// Pipe
import { CurrencyFormatPipe } from 'src/app/pipes/currency-format.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    PaymentComponent,
    ProductComponent,
    CardHomeComponent,
    CustomerComponent,
    NotfoundComponent,
    LoginPageComponent,
    CurrencyFormatPipe,
    TransactionComponent,
    AdvertisementComponent,
    DetailProductComponent,
    DetailCustomerComponent,
    AddEditProductComponent,
    DetailTransactionComponent,
    AddEditAdvertisementComponent,
  ],
  imports: [
    NgbModule,
    TagModule,
    CardModule,
    ToastModule,
    TableModule,
    FormsModule,
    ChartModule,
    CommonModule,
    EditorModule,
    ButtonModule,
    DialogModule,
    BrowserModule,
    DividerModule,
    TooltipModule,
    SkeletonModule,
    GalleriaModule,
    CalendarModule,
    PasswordModule,
    InputTextModule,
    AccordionModule,
    AppRoutingModule,
    HttpClientModule,
    FileUploadModule,
    InputNumberModule,
    InputSwitchModule,
    InputTextareaModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    ProgressSpinnerModule,
    BrowserAnimationsModule,
  ],
  providers: [ProductService, MessageService, ConfirmationService],
  bootstrap: [AppComponent],
})
export class AppModule {}
