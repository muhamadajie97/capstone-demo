import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {

  constructor(private router: Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      const getToken = localStorage.getItem('token');
      const activePath = state.url;

      switch(activePath){
        case '/login':
          getToken ? this.router.navigate(['']) : true;
          return true
        default:
          getToken ? true : this.router.navigate(['login']);
          return true
      }
  }
}
