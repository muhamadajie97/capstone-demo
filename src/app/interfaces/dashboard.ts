export interface TransactionPerDay {
  count_pending: number;
  count_success: number;
  count_failed: number;
  count_expired: number;
  count_total_transaction: number;
}

export interface DataDashboard {
  active_user: number;
  product_count: number;
  transaction_sum_income: number;
  transaction_per_day: TransactionPerDay;
}

export interface Dashboard {
  status: string;
  message: string;
  data: DataDashboard;
}
