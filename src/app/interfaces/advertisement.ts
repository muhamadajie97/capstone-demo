export interface Advertisement {
    status: string;
    message: string;
    data: DataAdvertisement[];
}

export interface DataAdvertisement {
    name: string;
    image: string;
    id: number;
}