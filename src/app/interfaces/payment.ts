export interface Payment {
  status: string;
  message: string;
  data: dataPayment[];
}

export interface dataPayment {
  image_path: string;
  is_active: boolean;
  payment_method_types: string;
}
