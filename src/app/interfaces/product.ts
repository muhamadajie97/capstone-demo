export interface Product {
  status: string;
  message: string;
  data: Datum[];
  page?: number;
  size?: number;
  totalPage?: number;
  totalItem?: number;
}
export interface Datum {
  title: string;
  location: string;
  description: string;
  price: number;
  quota: number;
  image: Image[];
  travel_code: string;
  departure_date: string;
  return_date: string;
}
export interface Image {
  path: string;
  create_at: Date;
  update_at: Date;
  deleted_at?: any;
}
export interface ProductDetail {
  status: string;
  message: string;
  data: Datum;
}