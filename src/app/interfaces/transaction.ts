export interface Datum {
  invoice_number: string;
  transaction_date: Date;
  payment_status: string;
  payment_date: string;
}

export interface Transaction {
  status: string;
  message: string;
  data: Datum[];
  page: number;
  size: number;
  totalPage: number;
  totalItem: number;
}

// Transaction Detail

export interface GroupMember {
  fullname: string;
  phoneNumber: string;
  nik: string;
}

export interface DataTransactionDetail {
  title: string;
  fullname: string;
  group_member: GroupMember[];
  payment_method_types: string;
  payment_status: string;
  payment_link: string;
  invoice_id: string;
  ticket_qty: number;
  total_amount: number;
}

export interface TransactionDetail {
  status: string;
  message: string;
  data: DataTransactionDetail;
}
