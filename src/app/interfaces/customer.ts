export interface Customer {
    status: string;
    message: string;
    data: DataCustomer[];
    page: number;
    size: number;
    totalPage: number;
    totalItem: number
}

export interface DataCustomer {
    email: string;
    nik: string;
    username: string;
    dob: string;
    gender: string;
    fullname: string;
    phone_number: string;
}

export interface CustomerDetail {
    status: string;
    message: string;
    data: DataCustomer;
}

export interface CustomerTransactionDetail {
    status: string;
    message: string;
    data: CustomerTransactionStatus[];
    page: number;
    size: number;
    totalPage: number;
    totalItem: number
}

export interface CustomerTransactionStatus {
    id: string;
    title: string;
    departure_date: string;
    return_date: string;
    payment_status: string;
}